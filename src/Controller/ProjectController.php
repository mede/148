<?php

namespace App\Controller;

use App\Entity\Project;
use App\Form\RechercheType;
use App\Form\ProjectType;
use App\Repository\ProjectRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;


/**
 * @Route("/project")
 */
class ProjectController extends AbstractController
{
    /**
     * @Route("/{search}", name="project_show", methods={"GET"})
     * @return Response
     */
    public function projects(Request $request, ProjectRepository $projectRepository, PaginatorInterface $paginator, $search = null): Response
    {
        $form = $this->createForm(RechercheType::class);

        $projects = $projectRepository->searchByNomProjects($search);

        $pagination = $paginator->paginate(
            $projects, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            12  /*limit per page*/
        );
        dump($pagination);


        // parameters to template
        return $this->render('project/show.html.twig', [
            'pagination' => $pagination,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/search", name="project_search", methods={"POST","GET"})
     * @return Response
     */    public function search(Request $request,PaginatorInterface $paginator, ProjectRepository $projectRepository): Response

{
    $form = $this->createForm(\App\Form\RechercheType::class);
    $form->handleRequest($request);

$pagination = null;
       if ($form->isSubmitted() && $form->isValid()){

    $recherche = $form->getData('Recherche');

    $value = strtolower($recherche['Recherche']);

    $projects = $projectRepository->searchByNomProjects($value);

    $pagination = $paginator->paginate(
        $projects, /* query NOT result */
        $request->query->getInt('page', 1), /*page number*/
        12  /*limit per page*/
    );
       }
    // parameters to template
    return $this->render('project/show.html.twig', [
        'pagination' => $pagination,
        'form' => $form->createView()

    ]);

}

//    }
    /**
     * @Route("/new", name="project_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $project = new Project();
        $form = $this->createForm(ProjectType::class, $project);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($project);
            $entityManager->flush();

            return $this->redirectToRoute('project_index');
        }

        return $this->render('project/new.html.twig', [
            'project' => $project,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="projects_show", methods={"GET"})
     */
    public function show(Project $project): Response
    {
        return $this->render('project/show2.html.twig', [
            'project' => $project,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="project_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Project $project): Response
    {
        $form = $this->createForm(ProjectType::class, $project);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('project_index');
        }

        return $this->render('project/edit.html.twig', [
            'project' => $project,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="project_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Project $project): Response
    {
        if ($this->isCsrfTokenValid('delete'.$project->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($project);
            $entityManager->flush();
        }

        return $this->redirectToRoute('project_index');
    }
}

