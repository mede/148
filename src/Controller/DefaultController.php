<?php

namespace App\Controller;

use App\Form\RechercheType;
use App\Repository\DemandeRepository;
use App\Repository\ProjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default_index", methods={"GET"})
     */
    public function index(ProjectRepository $projectRepository): Response
    {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            $this->addFlash('success', 'Vous êtes connecté !');
            $user = $this->getUser();
            $project = $projectRepository->findByUser($user->getId());

            $form = $this->createForm(RechercheType::class);

            return $this->render('default/user.html.twig', [
                'project' => $project,
                'user' => $user,
                'form' => $form->createView()
            ]);
        }

        return $this->redirectToRoute('app_login');

    }
}
