<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Service\MailerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="register", methods={"POST"})
     */
    public function register( MailerInterface $mailer, MailerService $mailerService,Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {

        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        dump($form);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
            $user->setConfirmationToken($this->generateToken());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $token = $user->getConfirmationToken();
            $to = $user->getEmail();
            $subject = "Bienvenue";
            $fullName = $user->getPrenom()." ".$user->getNom();
            $body_text = "Nice to meet you {$fullName}! ️ \nClick on the link below to activate your account :\nhttp://localhost:8080/confirm/".$token;

            $mailerService->sendEmail($mailer, $to, $subject, $body_text);


            return $this->redirectToRoute('email');

        }
        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/confirm/{token}", name="confirm_account")
     * @param $token
     * @return Response
     */
    public function confirmAccount($token): Response
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findOneBy(['confirmationToken' => $token]);
        if($user) {
            $user->setConfirmationToken(null);
            $user->setIsVerified(true);
            $em->persist($user);
            $em->flush();
            $this->addFlash('confirm', 'success');
            return $this->redirectToRoute('app_login');
        } else {
            return $this->redirectToRoute('app_login');
        }
    }

    public function confirmMessage(){
        return $this->render("registration/confirmation.html.twig");
    }


    public function sendConfirmationToken(Request $request, MailerService $mailerService): \Symfony\Component\HttpFoundation\RedirectResponse
    {
        $em = $this->getDoctrine()->getManager();
        $email = $request->request->get('email');
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $email]);
        if($user === null) {
            $this->addFlash('not-user-exist', 'utilisateur non trouvé');
            return $this->redirectToRoute('back_user_new');
        }
        $user->setConfirmationToken($this->generateToken());
        $em->persist($user);
        $em->flush();
        $token = $user->getConfirmationToken();
        $to = $user->getEmail();
        $subject = "Activation de votre compte";
        $username = $user->getPrenom();
        $body_text = "Nice to meet you {$username}! ❤️ <br> Cliquez sur le lien ci-dessus pour activer votre compte <br>".$token;
        $mailerService->sendEmail($to, $subject, $body_text);
        return $this->redirectToRoute('back_niveau_index');
    }

    /**
     * @return string
     * @throws \Exception
     */


    /**
     * @return string
     * @throws \Exception
     */
    private function generateToken()
    {
        return rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');
    }
}
