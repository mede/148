<?php


namespace App\Service;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class MailerService extends AbstractController
{
    /**
     * @param $to
     * @param $subject
     * @param $body_text
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function sendEmail($mailer, $to, $subject, $body_text){

        $email = (new Email())
            ->from('leila53100@gmail.com')
            ->to($to)
            ->subject($subject)
            ->text($body_text);

        $mailer->send($email);
    }
}