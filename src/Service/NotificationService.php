<?php


namespace App\Service;


use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mailer\Bridge\Google\Transport\GmailSmtpTransport;


class NotificationService
{
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }



    public function send($text)
    {

        $transport = new GmailSmtpTransport('contact.olexcool@gmail.com', 'Olexcool2019');
        $mailer = new Mailer($transport);

        $email = (new Email())
            ->from('contact.olexcool@gmail.com')
            ->to('leila53100@gmail.com')
            ->subject('Time for Symfony Mailer!')
            ->context([
                'text' => $text,
            ]);

        $mailer->send($email);
    }
}